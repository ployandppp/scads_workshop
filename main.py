import pandas as pd
from sklearn.model_selection import train_test_split
from apyori import apriori
import argparse

def load_data(filename):
    data = pd.read_csv(filename)
    return data


def preprocess(data):
        
    # print(data) #python name convention start by lower case and connect by underscore
    item_list = data.unstack().dropna().unique()
    # print(item_list)
    # print("# items:", len(item_list))

    train, test = train_test_split(data,test_size=.10)
    # print("train:",train)
    # print("test:",test)

    train = train.T.apply(lambda x: x.dropna().tolist()).tolist()
    # print("transform")
    # for i in train[:10]:
    #     print(i)
    return train,test

def model(train,min_support=0.0045,min_confidence=0.2, min_lift=3, min_length=2):
    results = list(apriori(train,min_support=min_support, min_confidence=min_confidence, min_lift=min_lift, min_length=min_length))
    # print(results)


    return results

def visualize(results):
    for item in results:
        print(item)
        print("-------")

def argument():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data',type=str,help="csv as a dataset")
    parser.add_argument('--min_length','-m',type=int,default=3,help="minimum number of product")
    return parser

if __name__ =="__main__":
    parser = argument()   
    args = parser.parse_args()
    print(args.data)
    print(args.min_length)
    data = load_data(args.data)

    train,test = preprocess(data)
    results = model(train,min_length=args.min_length)
    visualize(results)
